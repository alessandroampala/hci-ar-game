using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    [SerializeField] private Transform target;
    private Vector3 currentVelocity;

    private void Start()
    {
        Vector3 targetDir = (target.transform.position - transform.position).normalized;
        targetDir.y = 0; //prevents rotation on the x-axis
        transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);
    }

    void Update()
    {
        Vector3 targetDir = (target.transform.position - transform.position).normalized;
        targetDir.y = 0; //prevents rotation on the x-axis
        Vector3 currentDir = Vector3.SmoothDamp(transform.forward, targetDir, ref currentVelocity, .25f);
        transform.rotation = Quaternion.LookRotation(currentDir, Vector3.up);
    }
}

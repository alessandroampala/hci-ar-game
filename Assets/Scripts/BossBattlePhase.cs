using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattlePhase : MonoBehaviour
{
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject healthBar;
    [SerializeField] private GameObject playerHealthBar;
    [SerializeField] private GameObject shootButton;


    public void InitializePhase()
    {
        StartCoroutine(InitializePhaseCoroutine());
    }

    public void ExitPhase()
    {
        GameManager.Instance.messageBox.SetActive(true);
        healthBar.SetActive(false);
        playerHealthBar.SetActive(false);
        shootButton.SetActive(false);
    }

    public void EndGameInitializePhase()
    {
        StartCoroutine(EndGameInitializePhaseCoroutine());
    }

    private IEnumerator EndGameInitializePhaseCoroutine()
    {
        yield return GameManager.Instance.ShowDialogue("end_game_dialogue");
        Application.Quit();
    }

    private IEnumerator InitializePhaseCoroutine()
    {
        yield return StartCoroutine(GameManager.Instance.ShowDialogue("boss_fight_dialogue"));

        GameManager.Instance.messageBox.SetActive(false);
        boss.SetActive(true);
        healthBar.SetActive(true);
        playerHealthBar.SetActive(true);
        shootButton.SetActive(true);
        boss.transform.position = GameManager.Instance.Origin.transform.position + Vector3.up * .5f;
        boss.transform.parent = GameManager.Instance.Origin.transform.parent;
    }

    public void Reset()
    {
        GameManager.Instance.messageBox.SetActive(true);
        boss.SetActive(false);
        healthBar.SetActive(false);
        playerHealthBar.SetActive(false);
        shootButton.SetActive(false);
    }
}
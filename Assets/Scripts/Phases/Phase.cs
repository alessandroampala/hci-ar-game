using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

[System.Serializable]
public class Phase
{
    public string phaseName;
    public UnityEvent enterActions;
    public UnityEvent exitActions;

    public virtual void Enter()
    {
        enterActions.Invoke();
    }
    public virtual void Exit()
    {
        exitActions.Invoke();
    }
}

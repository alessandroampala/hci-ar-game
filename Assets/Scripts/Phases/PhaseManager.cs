using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PhaseManager
{
    public List<Phase> phases;
    int currentPhase = 0;

    public void Init()
    {
        phases[currentPhase].Enter();
    }

    public void NextPhase()
    {
        phases[currentPhase].Exit();
        phases[++currentPhase].Enter();
    }
}

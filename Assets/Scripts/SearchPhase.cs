using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchPhase : MonoBehaviour
{
    [SerializeField] private GameObject pathNodePrefab;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float maxDistance = 10f;
    [SerializeField] private float emissionIntensity = 2f;
    [SerializeField] private float cubeReachedDistance = 1f;
    [SerializeField] private GameObject arrow;
    [SerializeField] private GameObject wall;

    Vector3[] path;
    int cubePathIndex = 0;
    GameObject currentCube;
    Material currentCubeMaterial;

    bool areaTracked = false;

    void Start()
    {
        GameManager.Instance.AreaTracked.AddListener(AreaTracked);
        Vector3 prevPos = Vector3.zero;
    }

    public void InitializePhase()
    {
        arrow.SetActive(true);
        StartCoroutine(GameManager.Instance.ShowDialogue("search_phase_dialogue"));
    }

    public void ExitPhase()
    {
        Destroy(currentCube);
        arrow.SetActive(false);
        wall.SetActive(false);
        this.enabled = false;
    }

    void Update()
    {
        if(areaTracked)
            UpdateCubesColor();
    }

    void AreaTracked(GameObject meshObject)
    {
        path = GeneratePathInsideMesh(5, meshObject);
        areaTracked = true;

        currentCube = Instantiate(pathNodePrefab, path[cubePathIndex], Quaternion.identity, GameManager.Instance.Origin.transform);
        currentCubeMaterial = currentCube.GetComponent<Renderer>().material;
        GameManager.Instance.TargetUpdated.Invoke(currentCube);
    }

    void UpdateCubesColor()
    {
        Color albedoColor = currentCubeMaterial.color;
        Color minAlbedoColor = new Color(albedoColor.r, albedoColor.g, albedoColor.b, 0);
        Color emissionColor = currentCubeMaterial.GetColor("_EmissionColor");


        //calculate color based on player distance
        float playerDistance = Vector3.Distance(currentCube.transform.position, playerTransform.position);

        float distanceLinearParameter = Mathf.InverseLerp(maxDistance, 0, playerDistance);

        emissionIntensity = Mathf.Lerp(0, 2, distanceLinearParameter);
        Color c = Color.Lerp(minAlbedoColor * emissionIntensity, albedoColor * emissionIntensity, distanceLinearParameter);
        currentCubeMaterial.SetColor("_EmissionColor", c);
        albedoColor.a = distanceLinearParameter;
        currentCubeMaterial.color = albedoColor;

        if (playerDistance < cubeReachedDistance)
        {
            if(cubePathIndex == path.Length - 1)
            {
                GameManager.Instance.phaseManager.NextPhase();
                return;
            }
            cubePathIndex++;
            Destroy(currentCube);
            currentCube = Instantiate(pathNodePrefab, path[cubePathIndex], Random.rotation, GameManager.Instance.Origin.transform);
            currentCubeMaterial = currentCube.GetComponent<Renderer>().material;

            GameManager.Instance.TargetUpdated.Invoke(currentCube);
        }
    }

    //Generates path with nodes within the bounding box of the mesh
    Vector3[] GeneratePathInsideMesh(int nodesNumber, GameObject meshObject)
    {
        Vector3[] path = new Vector3[nodesNumber];

        Matrix4x4 localToWorld = GameManager.Instance.Origin.transform.localToWorldMatrix;

        for (int i = 0; i < nodesNumber; i++)
        {
            path[i] = GeneratePointInsideMesh(meshObject);
        }

        return path;
    }

    Vector3 GeneratePointInsideMesh(GameObject meshObject)
    {
        Vector3 point;

        Renderer renderer = meshObject.GetComponent<Renderer>();
        point.x = Random.Range(renderer.bounds.min.x, renderer.bounds.max.x);
        point.y = Random.Range(renderer.bounds.min.y, renderer.bounds.max.y);
        point.z = Random.Range(renderer.bounds.min.z, renderer.bounds.max.z);

        return point;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTracking : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject wall;
    [SerializeField] private Material wallMaterial;
    [SerializeField] private bool drawTopFace;
    [SerializeField] private bool drawBottomFace;
    [SerializeField] private GameObject setVertexButton;
    [SerializeField] [Range(0, 10)] private float ceilingOffsetFromCamera = .5f;
    [SerializeField] [Range(0,-10)] private float floorOffsetFromCamera = -.5f;


    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh mesh;
    private Vector3[] areaVerteces = new Vector3[4];
    private int index = 0;

    private int verticesIndex = 0;
    private int trianglesIndex = 0;
    private Vector3[] verteces = new Vector3[8];
    private int[] triangles = new int[24 + 6 + 6];

    private float ceilingY, floorY;

    void Start()
    {
        meshRenderer = wall.GetComponent<MeshRenderer>();
        meshFilter = wall.GetComponent<MeshFilter>();

        mesh = new Mesh();
        mesh.name = "Wall Mesh";
        meshFilter.mesh = mesh;

        meshRenderer.material = wallMaterial;
    }

    public void InitializePhase()
    {
        setVertexButton.SetActive(true);
        wall.transform.parent = GameManager.Instance.Origin.transform;
        StartCoroutine(GameManager.Instance.ShowDialogue("area_tracking_dialogue"));
    }

    public void SetMeshVertex()
    {
        if(index == 0)
        {
            ceilingY = Camera.current.transform.position.y + ceilingOffsetFromCamera;
            floorY = Camera.current.transform.position.y + floorOffsetFromCamera;
        }

        if (index < areaVerteces.Length)
        {
            areaVerteces[index] = playerTransform.position;
            Debug.Log(areaVerteces[index]);
            index++;
            if(index == 2)
            {
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 2].x, floorY, areaVerteces[index - 2].z);
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 2].x, ceilingY, areaVerteces[index - 2].z);
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 1].x, floorY, areaVerteces[index - 1].z);
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 1].x, ceilingY, areaVerteces[index - 1].z);

                triangles[trianglesIndex++] = verticesIndex - 4;
                triangles[trianglesIndex++] = verticesIndex - 3;
                triangles[trianglesIndex++] = verticesIndex - 2;
                triangles[trianglesIndex++] = verticesIndex - 2;
                triangles[trianglesIndex++] = verticesIndex - 3;
                triangles[trianglesIndex++] = verticesIndex - 1;

                mesh.Clear();
                mesh.vertices = verteces;
                mesh.triangles = triangles;
                mesh.RecalculateNormals();
            }
            else if (index > 2) //create wall
            {
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 1].x, floorY, areaVerteces[index - 1].z);
                verteces[verticesIndex++] = new Vector3(areaVerteces[index - 1].x, ceilingY, areaVerteces[index - 1].z);
                
                triangles[trianglesIndex++] = verticesIndex - 4;
                triangles[trianglesIndex++] = verticesIndex - 3;
                triangles[trianglesIndex++] = verticesIndex - 2;
                triangles[trianglesIndex++] = verticesIndex - 2;
                triangles[trianglesIndex++] = verticesIndex - 3;
                triangles[trianglesIndex++] = verticesIndex - 1;

                //all 4 points obtained, complete the mesh
                if(index == areaVerteces.Length)
                {
                    //last side face
                    int lastPoligonIndex = trianglesIndex;
                    triangles[trianglesIndex++] = 0;
                    triangles[trianglesIndex++] = triangles[lastPoligonIndex - 3];
                    triangles[trianglesIndex++] = triangles[lastPoligonIndex - 1];
                    triangles[trianglesIndex++] = 0;
                    triangles[trianglesIndex++] = triangles[lastPoligonIndex - 1];
                    triangles[trianglesIndex++] = 1;

                    if(drawTopFace)
                    {
                        //top face
                        triangles[trianglesIndex++] = 0 * 2 + 1;
                        triangles[trianglesIndex++] = 2 * 2 + 1;
                        triangles[trianglesIndex++] = 1 * 2 + 1;
                        triangles[trianglesIndex++] = 0 * 2 + 1;
                        triangles[trianglesIndex++] = 3 * 2 + 1;
                        triangles[trianglesIndex++] = 2 * 2 + 1;
                    }
                    
                    if(drawBottomFace)
                    {
                        //bottom face
                        triangles[trianglesIndex++] = 0 * 2;
                        triangles[trianglesIndex++] = 1 * 2;
                        triangles[trianglesIndex++] = 2 * 2;
                        triangles[trianglesIndex++] = 0 * 2;
                        triangles[trianglesIndex++] = 2 * 2;
                        triangles[trianglesIndex++] = 3 * 2;
                    }

                    mesh.Clear();
                    mesh.vertices = verteces;
                    mesh.triangles = triangles;
                    mesh.RecalculateNormals();

                    setVertexButton.SetActive(false);
                    GameManager.Instance.phaseManager.NextPhase();
                    
                    GameManager.Instance.AreaTracked.Invoke(wall);
                }

                mesh.Clear();
                mesh.vertices = verteces;
                mesh.triangles = triangles;
                mesh.RecalculateNormals();

            }
        }
    }

}

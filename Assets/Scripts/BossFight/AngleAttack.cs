using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AngleAttack
{
    [Range(0, 360)] public float horizontalAngle;
    public float HorizontalAngle
    {
        get => horizontalAngle;
        set => horizontalAngle = value % 360;
    }

    [Range(0, 360)] public float verticalAngle;
    public float VerticalAngle
    {
        get => verticalAngle;
        set => verticalAngle = value % 360;
    }

    public int rows;
    public int columns;
    public float timeBetweenAttacks;
    public AttackMode attackMode;
    public float projectileForce = 500f;

    Vector2[,] positions;

    public AngleAttack(int rows, int columns, float horizontalAngle, float verticalAngle,
        float timeBetweenRowsOrColumns, AttackMode attackMode = AttackMode.Uniform)
    {
        this.rows = rows;
        this.columns = columns;
        this.HorizontalAngle = horizontalAngle;
        this.VerticalAngle = verticalAngle;
        this.timeBetweenAttacks = timeBetweenRowsOrColumns;
        this.attackMode = attackMode;
        positions = new Vector2[rows, columns];

        ComputeAngles();
    }

    public enum AttackMode
    {
        Uniform,
        LeftToRight,
        RightToLeft,
        UpDown,
        BottomUp,
    }

    public List<BossAction> ComputeAttackSequence()
    {
        positions = new Vector2[rows, columns];

        ComputeAngles();

        List<BossAction> sequence = new List<BossAction>();
        int iLength = positions.GetLength(0);
        int jLength = positions.GetLength(1);
        WaitAction waitAction = new WaitAction(timeBetweenAttacks);

        //correct angles
        for (int i = 0; i < iLength; i++)
        {
            for (int j = 0; j < jLength; j++)
            {
                positions[i, j].x -= verticalAngle / 2 - verticalAngle / rows / 2;
                positions[i, j].y -= horizontalAngle / 2 - horizontalAngle / columns / 2;
            }
        }

        switch (attackMode)
        {
            case AttackMode.Uniform:
                for (int i = 0; i < iLength; i++)
                {
                    for (int j = 0; j < jLength; j++)
                    {
                        sequence.Add(new AttackAction(positions[i, j]));
                    }
                }
                sequence.Add(waitAction);
                break;
            case AttackMode.UpDown:
                for (int j = 0; j < jLength; j++)
                {
                    for (int i = 0; i < iLength; i++)
                    {
                        sequence.Add(new AttackAction(positions[i, j]));
                    }
                    sequence.Add(waitAction);
                }
                break;
            case AttackMode.BottomUp:
                for (int j = jLength - 1; j >= 0; j--)
                {
                    for (int i = 0; i < iLength; i++)
                    {
                        sequence.Add(new AttackAction(positions[i, j]));
                    }
                    sequence.Add(waitAction);
                }
                break;
            case AttackMode.RightToLeft:
                for (int i = 0; i < iLength; i++)
                {
                    for (int j = 0; j < jLength; j++)
                    {
                        sequence.Add(new AttackAction(positions[i, j]));
                    }
                    sequence.Add(waitAction);
                }
                break;
            case AttackMode.LeftToRight:
                for (int i = iLength - 1; i >= 0; i--)
                {
                    for (int j = 0; j < jLength; j++)
                    {
                        sequence.Add(new AttackAction(positions[i, j]));
                    }
                    sequence.Add(waitAction);
                }
                break;
        }


        return sequence;
    }

    void ComputeAngles()
    {
        float horizontalStep = horizontalAngle / columns;
        float verticalStep = verticalAngle / rows;

        float[] horizontalAngles = new float[columns];
        float[] verticalAngles = new float[rows];

        for (int i = 0; i < horizontalAngles.Length; i++)
        {
            horizontalAngles[i] = i * horizontalStep;
        }

        for (int i = 0; i < verticalAngles.Length; i++)
        {
            verticalAngles[i] = i * verticalStep;
        }

        
        for (int i = 0; i < positions.GetLength(0); i++)
        {
            for (int j = 0; j < positions.GetLength(1); j++)
            {
                positions[i, j].x = verticalAngles[i];
                positions[i, j].y = horizontalAngles[j];
            }
        }
    }
}

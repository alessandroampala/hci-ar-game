using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    [SerializeField] GameObject attackPrefab;
    [SerializeField] List<AngleAttack> attackConfigurations;
    [SerializeField] float attackDistanceOffset = 1f;
    [SerializeField] float vulnerableTime = 5f;
    [SerializeField] float waitTimeAttackCycle = 3f;
    [SerializeField] Transform target;
    [SerializeField] Slider healthBarSlider;
    [SerializeField] float health = 200;

    List<List<BossAction>> actions = new List<List<BossAction>>();
    WaitForSeconds cycleWaitTime;
    VulnerableAction vulnerableAction;
    [HideInInspector] public bool isVulnerable = false;
    Animator animator;

    private float maxHealth;
    private Coroutine attackCoroutine;
    private bool passedByStart = false;


    void Start()
    {
        maxHealth = health;
        for (int i = 0; i < attackConfigurations.Count; i++)
        {
            actions.Add(attackConfigurations[i].ComputeAttackSequence());
        }

        cycleWaitTime = new WaitForSeconds(waitTimeAttackCycle);
        animator = GetComponent<Animator>();
        vulnerableAction = new VulnerableAction(animator, vulnerableTime);

        attackCoroutine = StartCoroutine(AttacksCoroutine());
        passedByStart = true;
    }

    void OnEnable()
    {
        if(passedByStart)
            attackCoroutine = StartCoroutine(AttacksCoroutine());
    }

    Vector3 currentVelocity = Vector3.zero;

    void Update()
    {
        Vector3 targetDir = (target.transform.position - transform.position).normalized;
        Vector3 currentDir = Vector3.SmoothDamp(transform.forward, targetDir, ref currentVelocity, .25f);
        transform.rotation = Quaternion.LookRotation(currentDir, Vector3.up);
    }

    IEnumerator AttacksCoroutine()
    {
        while(true)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                for (int j = 0; j < actions[i].Count; j++)
                {
                    //we need to do this because of Instantiate() that can't be called from actions[i][j].Execute()
                    if (actions[i][j].GetType() == typeof(AttackAction))
                    {
                        AttackAction attackAction = (AttackAction)actions[i][j];
                        Quaternion rotation = Quaternion.Euler(attackAction.angle);

                        Vector3 dir = (Quaternion.AngleAxis(attackAction.angle.x, transform.up) * Quaternion.AngleAxis(attackAction.angle.y, transform.right)) * transform.forward;

                        GameObject g = Instantiate(attackPrefab,
                            transform.position + dir * attackDistanceOffset,
                            Quaternion.AngleAxis(attackAction.angle.x, transform.up) * Quaternion.AngleAxis(attackAction.angle.y, transform.right));

                        Rigidbody rb = g.AddComponent<Rigidbody>();
                        rb.useGravity = false;
                        rb.AddForce(dir * attackConfigurations[i].projectileForce);

                        Destroy(g, 10f);
                    }
                    else
                        yield return StartCoroutine(actions[i][j].Execute(this));
                }
            }
            yield return StartCoroutine(vulnerableAction.Execute(this));
            yield return cycleWaitTime;
        }
    }

    public void TakeDamage(float amount)
    {
        if(isVulnerable)
        {
            health -= amount;
            healthBarSlider.value = Mathf.InverseLerp(0, maxHealth, health);
            if(health <= 0)
            {
                Die();
            }
        }
    }

    void Die()
    {
        StopCoroutine(attackCoroutine);
        animator.SetTrigger("death");
    }

    void OnDeathAnimationEnd()
    {
        gameObject.SetActive(false);
        GameManager.Instance.phaseManager.NextPhase();
    }

    public void Reset()
    {
        health = maxHealth;
        healthBarSlider.value = Mathf.InverseLerp(0, maxHealth, health);
        animator.Play("Base Layer.Animation");
    }
}

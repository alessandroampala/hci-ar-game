using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VulnerableAction : BossAction
{
    Animator animator;
    WaitForSeconds vulnerableTime;


    public VulnerableAction(Animator animator, float vulnerableTime)
    {
        this.animator = animator;
        this.vulnerableTime = new WaitForSeconds(vulnerableTime);
    }

    public override IEnumerator Execute(Boss boss)
    {
        boss.isVulnerable = true;
        animator.SetBool("vulnerable", boss.isVulnerable);
        yield return vulnerableTime;
        boss.isVulnerable = false;
        animator.SetBool("vulnerable", boss.isVulnerable);
    }
}

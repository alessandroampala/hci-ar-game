using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BossAction
{
    public abstract IEnumerator Execute(Boss boss);
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** This class is just a placeholder for the type */

public class AttackAction : BossAction
{
    public Vector2 angle;
    public AttackAction(Vector2 angle)
    {
        this.angle = angle;
    }

    public override IEnumerator Execute(Boss boss)
    {
        yield return null;
    }

}

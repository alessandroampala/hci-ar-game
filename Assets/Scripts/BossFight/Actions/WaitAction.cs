using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAction : BossAction
{
    private WaitForSeconds waitTime;

    public WaitAction(float waitTime)
    {
        this.waitTime = new WaitForSeconds(waitTime);
    }

    public override IEnumerator Execute(Boss boss)
    {
        yield return waitTime;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private GameObject target;

    private void Update()
    {
        if (target)
        {
            transform.LookAt(target.transform);
        }
    }

    public void SetTarget(GameObject newTarget)
    {
        this.target = newTarget;
    }
}

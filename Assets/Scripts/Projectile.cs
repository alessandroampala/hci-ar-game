using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damage;
    private MeshRenderer graphics;

    private ParticleSystem ps;
    private Rigidbody rb;
    private static float waitBeforeDestroyTime;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        waitBeforeDestroyTime = ps.main.startLifetime.constant;
        graphics = GetComponentInChildren<MeshRenderer>();
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 10f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Boss boss = collision.gameObject.GetComponent<Boss>();
        if(boss && boss.isVulnerable)
        {
            boss.TakeDamage(damage);

            //particle effect and destroy projectile
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.detectCollisions = false;
            graphics.enabled = false;
            ps.Play();
        }

        Destroy(gameObject, waitBeforeDestroyTime);

    }
}

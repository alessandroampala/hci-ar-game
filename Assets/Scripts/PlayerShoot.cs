using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField] float health = 100;
    [SerializeField] GameObject projectile;
    [SerializeField] Transform shootPoint;
    [SerializeField] float shootForce;
    [SerializeField] float shootRatio;
    [SerializeField] Slider healthBarSlider;
    [SerializeField] Animator deathTransitionAnimator;
    public UnityEvent playerDeath;

    float nextTimeToShoot = 0;
    float shootStep;
    private float maxHealth;

    void Start()
    {
        maxHealth = health;
        shootStep = 1f / shootRatio;
    }

    public void Shoot()
    {
        if(Time.time >= nextTimeToShoot)
        {
            GameObject o = Instantiate(projectile, shootPoint.transform.position, Random.rotation);
            Rigidbody rb = o.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * shootForce);
            Destroy(o, 10f);
            nextTimeToShoot = Time.time + shootStep;
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        healthBarSlider.value = Mathf.InverseLerp(0, maxHealth, health);
        if (health <= 0)
            Die();
    }

    private void Die()
    {
        deathTransitionAnimator.SetTrigger("death");
        playerDeath.Invoke();
    }

    public void Reset()
    {
        health = maxHealth;
        healthBarSlider.value = Mathf.InverseLerp(0, maxHealth, health);
    }
}

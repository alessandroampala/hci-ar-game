using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get => instance;
    }

    [SerializeField] GameObject originPrefab;
    [SerializeField] GameObject transparentOriginPrefab;
    [SerializeField] Button setOriginButton;
    [SerializeField] ARRaycastManager m_RaycastManager;
    [SerializeField] ARPlaneManager m_PlaneManager;
    [SerializeField] ARAnchorManager m_AnchorManager;
    [SerializeField] public GameObject messageBox;
    [SerializeField] Transform target;


    /*[SerializeField] private DialogueCloud cloud;
    private NarratorDialogueSystem dialogueSystem;*/


    [System.Serializable]
    public class UnityEventMesh : UnityEvent<GameObject>
    {
    }
    [HideInInspector] public UnityEventMesh AreaTracked = new UnityEventMesh();

    [System.Serializable]
    public class UnityEventGameObject : UnityEvent<GameObject>
    {
    }
    public UnityEventGameObject TargetUpdated = new UnityEventGameObject();

    List<ARRaycastHit> m_Hits = new List<ARRaycastHit>();
    Pose hitPose;
    ARPlane hitPlane;
    bool placed = false;

    GameObject transparentOrigin;
    GameObject origin;
    public GameObject Origin
    {
        get => origin;
    }

    [SerializeField] GameObject camera;

    public PhaseManager phaseManager;

    [HideInInspector] public NarratorDialogueSystem dialogueSystem;
    [HideInInspector] public DialogueCloud cloud;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        dialogueSystem = GetComponent<NarratorDialogueSystem>();
        cloud = GetComponent<DialogueCloud>();

        transparentOrigin = Instantiate(transparentOriginPrefab, Vector3.zero, Quaternion.identity);
        transparentOrigin.SetActive(false);
        phaseManager.Init();
    }

    Vector3 currentVelocity;

    void Update()
    {
        if(!placed)
        {
            PreviewOrigin();
        }
    }

    void PreviewOrigin()
    {
        Vector2 middleScreenPoint = new Vector2(Screen.width / 2, Screen.height / 2);

        if (m_RaycastManager.Raycast(middleScreenPoint, m_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            hitPose = m_Hits[0].pose;
            var hitTrackableId = m_Hits[0].trackableId;
            hitPlane = m_PlaneManager.GetPlane(hitTrackableId);

            // This attaches an anchor to the area on the plane corresponding to the raycast hit,
            // and afterwards instantiates an instance of your chosen prefab at that point.
            // This prefab instance is parented to the anchor to make sure the position of the prefab is consistent
            // with the anchor, since an anchor attached to an ARPlane will be updated automatically by the ARAnchorManager as the ARPlane's exact position is refined.
            transparentOrigin.SetActive(true);
            setOriginButton.interactable = true;
            transparentOrigin.transform.position = m_Hits[0].pose.position;
        }
        else
        {
            transparentOrigin.SetActive(false);
            setOriginButton.interactable = false;
        }
    }

    public void PlaceOrigin()
    {
        ARAnchor anchor = m_AnchorManager.AttachAnchor(hitPlane, hitPose);

        if (anchor == null)
            Debug.Log("Error creating anchor.");
        else
        {
            Destroy(transparentOrigin);
            origin = Instantiate(originPrefab, anchor.transform);
            placed = true;
            setOriginButton.gameObject.SetActive(false);
            m_PlaneManager.enabled = false;

            messageBox.SetActive(true);
            messageBox.transform.position = anchor.transform.position;
            messageBox.transform.parent = anchor.transform;
            messageBox.transform.position += Vector3.up * .5f;


            phaseManager.NextPhase();
        }
    }

    public IEnumerator ShowDialogue(string dialogueName)
    {
        cloud.Dialogue = dialogueSystem.GetDialogue(dialogueName);

        while(!cloud.HasDialogueEnded())
        {
            cloud.Next();
            yield return new WaitForSeconds(cloud.currentLineWriteTime);
        }
    }
    
}

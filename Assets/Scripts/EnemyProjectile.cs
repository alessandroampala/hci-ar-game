using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float damage = 10;

    private ParticleSystem ps;

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        Destroy(gameObject, 10f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        PlayerShoot player = collision.gameObject.GetComponent<PlayerShoot>();
        if (player)
        {
            player.TakeDamage(damage);
            ps.Play();
            Destroy(gameObject);
        }        
    }
}

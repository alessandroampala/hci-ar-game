using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;

public class DialogueCloud : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI uiText;
    private Coroutine currentCoroutine;

    private IEnumerable<string> dialogue;
    public IEnumerable<string> Dialogue
    {
        set
        {
            dialogue = value;
            iterator = dialogue.GetEnumerator();
        }
    }
    private IEnumerator<string> iterator;
    [HideInInspector] public float currentLineWriteTime = 0f;


    [Min(0)] public float waitTimeBetweenLetters = .1f;

    public void Clear()
    {
        if (currentCoroutine != null)
            StopCoroutine(currentCoroutine);
        uiText.text = "";
        iterator = null;
    }

    public void Next()
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            currentCoroutine = null;
            uiText.text = iterator.Current;
        }
        else if (iterator.MoveNext())
        {
            currentLineWriteTime = waitTimeBetweenLetters * iterator.Current.Length;
            ShowLineAnimated(iterator.Current);
        }
        else
            Clear();
    }

    public bool HasDialogueEnded()
    {
        return iterator == null;
    }

    public void ShowLineAnimated(string line)
    {
        currentCoroutine = StartCoroutine(ShowLineAnimatedCoroutine(line));
    }

    private IEnumerator ShowLineAnimatedCoroutine(string line)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Capacity = line.Length;
        WaitForSeconds waitTime = new WaitForSeconds(waitTimeBetweenLetters);

        for (int i = 0; i < line.Length; i++)
        {
            stringBuilder.Append(line[i]);
            uiText.text = stringBuilder.ToString();
            yield return waitTime;
        }
        currentCoroutine = null;
    }
}

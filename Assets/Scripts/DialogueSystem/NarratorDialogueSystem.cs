using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Dialogue
{
    public string name;
    public string[] lines;

    public IEnumerable<string> GetLines()
    {
        for (int i = 0; i < lines.Length; i++)
            yield return lines[i];
    }
}

public class NarratorDialogueSystem : MonoBehaviour
{
    [SerializeField] private Dialogue[] dialogues;
    private Dictionary<string, Dialogue> dialoguesDict = new Dictionary<string, Dialogue>();
    
    private void Awake()
    {
        for(int i = 0; i < dialogues.Length; i++)
        {
            dialoguesDict[dialogues[i].name] = dialogues[i];
        }
    }

    public IEnumerable<string> GetDialogue(string dialogueName)
    {
        Debug.Assert(dialoguesDict.ContainsKey(dialogueName));
        return dialoguesDict[dialogueName].GetLines();
    }
}

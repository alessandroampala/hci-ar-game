# Introduction
This is an Augmented Reality game developed using Unity's [AR Foundation](https://unity.com/unity/features/arfoundation).

# Key Features
- Phase Manager
- Area Tracking
- (Narrator) Dialogue System
- Bullet Hell-like Attack System

# Assets used
For the boss battle, the Ophanim asset was taken from this [Sketchfab page](https://sketchfab.com/3d-models/ophanim-46d33d46e08848b3a53fc6e863c9cb38).
